require 'rails_helper'

RSpec.describe Channel, type: :model do
  subject(:channel) { Channel.new(name: 'Foo') }

  describe '#upper_name' do
    it 'returns  FOO for name Foo' do
      expect(channel.upper_name).to eq 'FOO'
    end
  end
end
