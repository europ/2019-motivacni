Rails.application.routes.draw do
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  resources :users
  root to: 'channels#index'

  resources :messages

  resources :channels do
    resources :messages
  end
end
